<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ccaa extends Migration
{

    public function up()
    {
        Schema::create('ccaa', function (Blueprint $table) {
            $table->id();
            $table->string('nombre')->unique();
            $table->integer('pais_id');
            $table->foreign('pais_id')->references('id')->on('paises')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('ccaa');
    }
}

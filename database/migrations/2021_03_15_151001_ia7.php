<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Ia7 extends Migration
{
    public function up()
    {
        Schema::create('ia7', function (Blueprint $table) {
            $table->id();
            $table->date('fecha');
            $table->integer('ccaas_id');
            $table->float('incidencia');
            $table->foreign('ccaas_id')->references('id')->on('ccaa')->onDelete('cascade');

        });
    }


    public function down()
    {
        Schema::dropIfExists('ia7');
    }
}

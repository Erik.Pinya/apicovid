<?php

use App\Http\Controllers\Ia14Controller;
use App\Http\Controllers\Ia7Controller;
use App\Http\Controllers\muertosController;
use App\Http\Controllers\casosController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});







// ia14
Route::resource('ia14',Ia14Controller::class);
Route::get('ia14/{fecha}',[Ia14Controller::class,'show']);
Route::get('/ia14ShowAll',[Ia14Controller::class,'showAll']);
Route::post('/ia14Store',[Ia14Controller::class,'store']);
Route::patch('/ia14Update',[Ia14Controller::class,'update']);
Route::delete('/ia14Destroy/{fecha}',[Ia14Controller::class,'destroy']);

Route::get('Ia14/{fecha}/{fecha2}',[Ia14Controller::class,'showCollection']);


// ia7
Route::resource('ia7',Ia7Controller::class);
Route::get('ia7/{fecha}',[Ia7Controller::class,'show']);
Route::get('/ia7ShowAll',[Ia7Controller::class,'showAll']);
Route::post('/ia7Add',[Ia7Controller::class,'store']);
Route::patch('/ia7Update',[Ia7Controller::class,'update']);
Route::delete('/ia7destroy/{fecha}',[Ia7Controller::class,'destroy']);

Route::get('ia7/{fecha}/{fecha2}',[Ia7Controller::class,'showCollection']);


// casos
Route::resource('Casos',CasosController::class);
Route::get('Casos/{fecha}',[CasosController::class,'show']);
Route::get('/CasosShowAll',[CasosController::class,'showAll']);
Route::post('/CasosAdd',[CasosController::class,'store']);
Route::patch('/CasosUpdate',[CasosController::class,'update']);
Route::delete('/Casosdestroy/{fecha}',[CasosController::class,'delete']);

Route::get('Casos/{fecha}/{fecha2}',[CasosController::class,'showCollection']);

// muertosss
Route::resource('Muertos',muertosController::class);
Route::get('Muertos/{fecha}',[muertosController::class,'show']);
Route::get('/MuertosShowAll',[muertosController::class,'showAll']);
Route::patch('/MuertosUpdate',[muertosController::class,'update']);
Route::delete('/Muertosdestroy/{fecha}',[muertosController::class,'delete']);
Route::post('/MuertosAdd',[muertosController::class,'store']);

Route::get('Muertos/{fecha}/{fecha2}',[muertosController::class,'showCollection']);



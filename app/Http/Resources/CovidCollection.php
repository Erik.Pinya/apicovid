<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class CovidCollection extends ResourceCollection
{
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(
                function ( $ia14 ) {
                    return new Ia14Resource($ia14);
                }
            )
        ];
    }
}

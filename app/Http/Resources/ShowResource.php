<?php

namespace App\Http\Resources;

use App\Models\CCAAs;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\DB;

class ShowResource extends JsonResource
{
    public function toArray($request)
    {

        $pais = DB::table('paises')
            ->join('ccaa', 'paises.id', '=', 'ccaa.pais_id')
            ->select('paises.*')
            ->first();

        $ccaa = ccaas::where('id', $this->ccaas_id)->first();
        return [
            'fecha' => $this->fecha,
            'ccaa' => $ccaa->nombre,
            'pais' => $pais->nombre,
            'media' => $this->incidencia,

        ];
    }
}
